﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImgVwr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public string ImageURI
        {
            get { return (string)GetValue(ImageURIProperty); }
            set { SetValue(ImageURIProperty, value); }
        }
        public static readonly DependencyProperty ImageURIProperty = DependencyProperty.Register("ImageURI", typeof(string), typeof(MainWindow));

        private Image CurrentImage
        {
            get
            {
                return new Image()
                {
                    Source = new BitmapImage(new Uri(ImageURI))
                };
            }
        }



        private Popup fullscreenPopup;


        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            string[] args = Environment.GetCommandLineArgs();

            if (File.Exists(args[0]))
            {
                // Image = args[0];
            }
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            OpenImage();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseImage();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OpenImage()
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                Title = "Open Image...",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png",
                ReadOnlyChecked = true
            };

            if (ofd.ShowDialog() == true)
            {
                this.SetCurrentValue(ImageURIProperty, ofd.FileName);
                // CurrentImageURI = ofd.FileName;
            }
        }

        private void CloseImage()
        {
            if (fullscreenPopup != null && fullscreenPopup.IsOpen)
            {
                ExitFullScreen();
            }
            ImageURI = null;
            fullscreenPopup = null;
        }

        private void NextImage()
        {

        }

        private void PrevImage()
        {

        }

        private void FullScreen()
        {
            fullscreenPopup = new Popup
            {
                Child = new Image
                {
                    Source = CurrentImage.Source,
                    Stretch = Stretch.UniformToFill,
                    Height = SystemParameters.WorkArea.Width,
                    Width = SystemParameters.WorkArea.Height
                }
            };
            fullscreenPopup.IsOpen = true;
        }

        private void ExitFullScreen()
        {
            fullscreenPopup.IsOpen = false;
            fullscreenPopup = null;
        }

        private void ShowHelp()
        {
            About about = new About();
            about.ShowInTaskbar = true;
            about.ShowDialog();
        }

        private void MetroWindow_KeyUp(object sender, KeyEventArgs e)
        {
            // CTRL + O
            if ((e.SystemKey == Key.LeftCtrl || e.SystemKey == Key.RightCtrl) && e.Key == Key.O)
            {
                OpenImage();
            }

            // Left Arrow
            if (e.Key == Key.Left)
            {
                PrevImage();
            }

            // Right Arrow
            if (e.Key == Key.Right)
            {
                NextImage();
            }

            // F1 - help
            if (e.Key == Key.F1)
            {
                ShowHelp();
            }

            // F11 - full screen
            if (e.Key == Key.F11)
            {
                FullScreen();
            }

            // ESC - exit full screen
            if (e.Key == Key.Escape)
            {
                ExitFullScreen();
            }
        }

        public delegate void OpenImageEventHandler(object sender, EventArgs e);
        public event OpenImageEventHandler openImage;
        protected void ImageOpened(EventArgs e)
        {
            if (openImage != null)
            {
                // Handle loading the image
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ImageURI != null)
            {
                CloseImage();
            }
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            ShowHelp();
        }
    }
}
